# REST API & structura Backend

Datele deservite pentru frontend sunt furnizate de catre API-ul REST dezvoltat in ExpressJS.

Structura de directoare a proiectului ExpressJS este aceasta:

- __\\angular\\backend__
   - __config__
     - [config.json](config/config.json)
     - [passport\-setup.js](config/passport-setup.js)
   - __migrations__
     - [20191227112831\-create\-user.js](migrations/20191227112831-create-user.js)
     - [20191231111737\-create\-playlist.js](migrations/20191231111737-create-playlist.js)
   - __models__
     - [index.js](models/index.js)
     - [playlist.js](models/playlist.js)
     - [user.js](models/user.js)
   - [node\_modules](node_modules)
   - [package\-lock.json](package-lock.json)
   - [package.json](package.json)
   - __routes__
     - [auth\-routes.js](routes/auth-routes.js)
     - [playlists\-routes.js](routes/playlists-routes.js)
   - __seeders__
   - [server.js](server.js)

In **config** sunt fisierele de configurare a ORM-ului Sequelize ( credentialele bazei de date si configurari specifice ) si a diverselor tipuri de middleware ( Passport.js - autentificare si autorizare ).

In **migrations** sunt fisierele de migrare pentru setarea tabelelor in mod automat folosind ORM-ul sequelize.

In **models** se regasesc entitatiile folosite pentru maparea ORM-ului pe baza de date si structura tabelelor ce vor deservi la persistenta datelor.

In **routes** sunt fisierele ce contin rutele ExpressJS ale API-ului, acestea sunt de forma /api/entitate sau /api/entitate/:id pentru efectuarea operatiilor CRUD si intoarcerea datelor necesare utilizatorului in interfata Angular.

In **seeders** sunt fisiere ce vor popula tabelele din baza de date ( be baza fisierelor din  **models**) cu date dummy pentru testare.

**Package.json** detine toate dependintele aplicatiei in termeni de middleware folosite.

**Server.js** este fisierul de intrare in aplicatia ExpressJS. Entry point-ul pentru backend cu setari de tip: pe ce port sa asculte serverul de backend, prefixarea rutelor, setarea middleware-urilor, etc.


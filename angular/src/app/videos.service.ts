import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Video } from './video';
import { Category } from './category';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  private videosUrl = 'http://localhost:3000/api/videos';
  private videoCategoriesUrl = 'http://localhost:3000/api/videoCategories';

  private changedVideoSource  = new Subject<Video[]>();
  private editedVideoSource   = new Subject<Video>();

  changedVideo$ = this.changedVideoSource.asObservable();
  editedVideo$  = this.editedVideoSource.asObservable();

  httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true
  };

  messages: string[] = [];

  constructor(
   private http: HttpClient
   ) { }


   changedVideo(video: Video[])
   {
       this.changedVideoSource.next(video);
   }

   editedVideo(video: Video)
   {
       this.editedVideoSource.next(video);
   }

   getVideosUrl(){
      return this.videosUrl;
   }

   getVideos(){
       return this.http.get<Video[]>(this.videosUrl, this.httpOptions)
              .toPromise()
              .then(data => { return data; });
   }

    getVideoCategories(){
        return this.http.get<Category[]>(this.videoCategoriesUrl, this.httpOptions)
             .toPromise()
             .then(data => { return data; });
    }

   putVideo(video){

        var obj = {
           "id" : video.videoId,
           "snippet": {}
        };
        for (let prop in video) {
            if(prop !== 'videoId'){
                obj['snippet'][prop] = video[prop];
            }
        }
        console.log('videoservice putVideo');
        let body = JSON.stringify(obj);
        console.log(body);
        return this.http.put<Video[]>(this.videosUrl, body, this.httpOptions)
               .toPromise()
               .then(data => { return data; });

   }

   deleteVideo(video: Video): Observable<{}>
   {

      this.videosUrl = this.videosUrl + '/' + video.videoId;
      return this.http
              .delete<Video>(this.videosUrl, this.httpOptions);
   }

   private log(message: string) {
      this.add(`VideoService: ${message}`);
   }

   /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
   private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
   }

   add(message: string) {
       this.messages.push(message);
   }

}

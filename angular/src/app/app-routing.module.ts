import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoogleLoginComponent } from './google-login/google-login.component';
import { ChannelsComponent } from './channels/channels.component'
import { PlaylistsComponent } from './playlists/playlists.component'
import { VideosComponent } from './videos/videos.component'
import { AppComponent } from './app.component'

const routes: Routes = [
 // { path: '', component: AppComponent },
  {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'
  },
  { path: 'login', component: GoogleLoginComponent },
  { path: 'channels', component: ChannelsComponent},
  { path: 'playlists', component: PlaylistsComponent},
  { path: 'videos', component: VideosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

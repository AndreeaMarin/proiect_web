import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';
import { NgModule } from '@angular/core';
import { FileSelectDirective } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';


import { AppComponent } from './app.component';
import { GoogleLoginComponent } from './google-login/google-login.component';
import { AppRoutingModule } from './app-routing.module';
import { ChannelsComponent } from './channels/channels.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { CookieService } from 'ngx-cookie-service';
import { SpinnerComponent } from './spinner/spinner.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { DataViewModule } from 'primeng/dataview';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PanelModule } from 'primeng/panel';
import { InplaceModule } from 'primeng/inplace';
import { DropdownModule } from 'primeng/dropdown';

import { VideosComponent } from './videos/videos.component';

import { VideoInterceptor } from './video.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    FileSelectDirective,
    GoogleLoginComponent,
    ChannelsComponent,
    PlaylistsComponent,
    SpinnerComponent,
    VideosComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    DialogModule,
    BrowserAnimationsModule,
    FileUploadModule,
    DataViewModule,
    PanelModule,
    InplaceModule,
    DropdownModule
  ],
  exports: [
    SpinnerComponent,
  ],
  providers: [
    CookieService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: VideoInterceptor,
        multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';

import { VideosService } from '../videos.service';
import { PlaylistsService } from '../playlists.service';

import { VideoInterceptor } from '../video.interceptor';

import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { Video } from '../video';
import { Category } from '../category';
import { Playlist } from '../playlist';

import {
  HttpParams
} from "@angular/common/http";


@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

  url: string;

  video: Video;

  videos: Video[];

  displayDialog: boolean;

  displayDialogVideoPlayer: boolean;

  isRequesting: boolean;

  selectedVideo: Video;

  videoUrl: SafeResourceUrl;

  categoryId: string;

  updatedVideo: {};

  videoCategories: Category[];

  playlistsToSelectFrom: Playlist[];

  displayDialogPlaylistSelector: boolean;

  selectedPlaylist: string;

  constructor(  private videosService: VideosService, private sanitizer: DomSanitizer, private playlistService: PlaylistsService, private ref: ChangeDetectorRef ) { }

  ngOnInit() {
      this.url=this.getVideosUrl();
      this.getVideos().then(videos => {
        this.videos = videos.map(video => ({ videoId: video.videoId, title: video.title, description: video.description, thumbnails: JSON.parse(video.thumbnails), categoryId: video.categoryId }));
        this.isRequesting = false;
      });
      this.getVideoCategories().then(categories => {
        this.videoCategories = categories.filter(category => category.snippet.assignable == true);
        this.isRequesting = false;
      });
      this.getPlaylistForVideos();
  }

  getVideos(){
      this.isRequesting = true;
      return this.videosService.getVideos();
  }

  getVideoCategories(){
      this.isRequesting = true;
      return this.videosService.getVideoCategories();
  }

  getPlaylistForVideos(){
      this.isRequesting = true;
      return this.playlistService.getPlaylists()
      .subscribe(
         data => this.playlistsToSelectFrom = data,
         error => this.handleError(error)
      );
  }

  getVideosUrl(){
      return this.videosService.getVideosUrl();
  }

  onSelect(event){
      this.video = {title:'',description:'',categoryId:0};
      this.displayDialog = true;
  }

  onUpload(event){
      this.getVideos().then(videos => {
        this.videos = videos.map(video => ({ videoId: video.videoId, title: video.title, description: video.description, thumbnails: JSON.parse(video.thumbnails), categoryId: video.categoryId }));
        this.isRequesting = false;
      });
      this.displayDialog = true;
  }

  selectVideo(event: Event, video: Video) {
      this.video = this.cloneVideo(video);
      this.selectedVideo = this.cloneVideo(video);
      this.displayDialogVideoPlayer = true;
      this.videoUrl =
        this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+this.selectedVideo.videoId);
      event.preventDefault();
  }

  save() {
     this.displayDialog = false;

     let params = new HttpParams();
     params = params.set('title', this.video.title);
     params = params.set('description', this.video.description);
     params = params.set('categoryId', this.categoryId);
     this.url = this.url + '?' + params.toString();
  }

  onDialogClose(){
      this.isRequesting = true;

      if(typeof this.updatedVideo !== 'undefined' && Object.keys(this.updatedVideo).length > 0){
          this.videosService.putVideo(this.updatedVideo).then(videos => {
              this.videos = videos.map(video => ({ videoId: video.videoId, title: video.title, description: video.description, thumbnails: JSON.parse(video.thumbnails) }));
              this.isRequesting = false;
              this.updatedVideo = {};
          });
      }else{
        this.isRequesting = false;
      }
  }

  updateField(event: Event, video: Video){
     let updatedVideo = this.diffVideo(video, this.video);
     if(Object.keys(updatedVideo).length > 0){
        this.updatedVideo = this.diffVideo(video, this.video);
        this.updatedVideo['videoId'] = video.videoId;
        this.updatedVideo['categoryId'] = this.video.categoryId;
     }
  }

  delete() {
      this.videosService.deleteVideo(this.video)
          .subscribe(videos => {
              let index = this.videos.indexOf(this.video);
              this.videos = this.videos.filter((val, i) => i != index);
          });

      let index = this.videos.indexOf(this.video);
      this.videos = this.videos.filter((val, i) => i != index);

      this.video = null;
      this.displayDialog = false;
  }

  cloneVideo(v: Video): Video {
      let video = {};
      for (let prop in v) {
          video[prop] = v[prop];
      }
      return video;
  }

  diffVideo(v1: Video, v2: Video): {} {
      let video = {};
      for (let prop in v1) {
          if(v1[prop] !== v2[prop]){
              video[prop] = v1[prop];
              continue;
          }
          video[prop] = v1[prop];
      }
      return video;
  }

  addVideoToPlaylist(event: Event, video: Video){
      this.displayDialogPlaylistSelector=true;
      this.selectedVideo = this.cloneVideo(video);
  }

  saveVideoToPlaylist(){
      this.displayDialogPlaylistSelector=false;
      this.playlistService.addVideoToPlaylist(this.selectedPlaylist, this.selectedVideo.videoId).subscribe((data)=>{console.log(data)});
  }

  handleResponse(videos: any) : void
  {
      this.videos = videos;
      this.isRequesting = false;
  }

  handleError(error: any): void
  {
      this.isRequesting = false;
      console.log(error);
  }

}

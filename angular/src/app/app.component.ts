import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular';

  user: User;

  constructor(  private loginService: LoginService, private cookieService: CookieService, private router: Router ) {   }

   ngOnInit() {
        this.getUser();
        this.isAuth();
   }

    getUser(): void {
        this.loginService.getUser()
            .subscribe(
                user => {
                    if(typeof user.userId !== 'undefined'){
                        this.user = user;
                    }else{
                        this.user = null;
                    }
                }
            );
    }

    deleteUser(): void {
        this.loginService.deleteUser()
            .subscribe(user => {
                this.user = user;
            }, (err) => {
                console.log(err);
            });

        this.cookieService.delete('connect.sid');
        this.router.navigate(['/login']);
    }

    isAuth(){
      if(this.authenticated()){
         this.router.navigate(['/channels']);
      }
    }

    authenticated(): boolean{
    		if (this.cookieService.get('connect.sid').trim().length > 0) {
            return true;
    		} else {
            return false;
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Channel } from './channel';

@Injectable({
  providedIn: 'root'
})
export class ChannelsService {

  private channelsUrl = 'http://localhost:3000/api/channels';  // URL to web api
  httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true
  };

  messages: string[] = [];

  constructor(
   private http: HttpClient
   ) { }

   getChannels(): Observable<Channel[]> {
      // TODO: send the message _after_ fetching the heroes
      return this.http.get<Channel[]>(this.channelsUrl,this.httpOptions)
            .pipe(
              tap(_ => this.log('fetched channels')),
              catchError(this.handleError<Channel[]>('getChannels', []))
            );
   }

   putChannel(channel: Channel): Observable<{}>
   {
      var obj = {
         "id" : channel.channelId,
         "brandingSettings" : {
             "channel": {
                "description": channel.description
             }
         }
      };
      let body = JSON.stringify(obj);
      console.log(body);
      return this.http
              .put<Channel>(this.channelsUrl, body, this.httpOptions);
   }

   /**
      * Handle Http operation that failed.
      * Let the app continue.
      * @param operation - name of the operation that failed
      * @param result - optional value to return as the observable result
      */
     private handleError<T> (operation = 'operation', result?: T) {
       return (error: any): Observable<T> => {

         // TODO: send the error to remote logging infrastructure
         console.error(error); // log to console instead

         // TODO: better job of transforming error for user consumption
         this.log(`${operation} failed: ${error.message}`);

         // Let the app keep running by returning an empty result.
         return of(result as T);
       };
     }

     add(message: string) {
        this.messages.push(message);
     }

     private log(message: string) {
       this.add(`ChannelsService: ${message}`);
     }
}

export class User {
  userId?: string;
  username?: string;
  accessToken?: string;
  refreshToken?: string;
}

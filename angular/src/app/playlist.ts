export class Playlist {
  playlistId?: string;
  title?: string;
  description?: string;
  channelId?: string;
  itemCount?: number;
}

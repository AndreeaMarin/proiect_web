import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { Injectable, Injector } from '@angular/core';

import { Observable } from "rxjs";

const VIDEOS_URL = "http://localhost:3000/api/videos";

@Injectable()
export class VideoInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
     if (req.url.indexOf(VIDEOS_URL) === 0 && req.method === 'POST') {
        var urlParams = new URLSearchParams(req.url.substr(32));
        const cloneReq = req.clone({
          url:"http://localhost:3000/api/videos",
          headers: new HttpHeaders(
              {
                  'X-Video-Title': urlParams.get('title') ,
                  'X-Video-Description': urlParams.get('description'),
                  'X-Video-Category': urlParams.get('categoryId')
              }

          )
        });
        return next.handle(cloneReq);
     }
     return next.handle(req);
  }
}

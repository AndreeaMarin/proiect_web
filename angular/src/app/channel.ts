export class Channel {
  channelId?: string;
  userId?: string;
  title?: string;
  description?: string;
  subscriberCount?: number;
}

import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { Channel } from '../channel';

import { ChannelsService } from '../channels.service';


@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.css']
})
export class ChannelsComponent implements OnInit {

  channels: Channel[];

  isRequesting: boolean;

  selectedChannel: Channel;

  newChannel: boolean;

  cols: any[];

  channel: Channel;

  displayDialog: boolean;

  constructor( private  channelService: ChannelsService ) {   }

  ngOnInit() {
     this.getChannels();
  }

  getChannels(): void {

    this.isRequesting = true;

    this.channelService.getChannels()
        .subscribe(
           data => this.handleResponse(data),
           error => this.handleError(error)
        );
  }

  showDialogToAdd() {
    this.newChannel = true;
    this.channel = {channelId:'',userId:'',title:'',description:'',subscriberCount:0};
    this.displayDialog = true;
  }

  onRowSelect(event) {
      this.newChannel = false;
      this.channel = this.cloneChannel(event.data);
      this.displayDialog = true;
  }

  cloneChannel(c: Channel): Channel {
        let channel = {};
        for (let prop in c) {
            channel[prop] = c[prop];
        }
        return channel;
  }

  save() {
        let channels = [...this.channels];

        this.channelService.putChannel(this.channel)
            .subscribe(channels => {
                channels[this.channels.indexOf(this.selectedChannel)] = this.channel;
            });
        channels[this.channels.indexOf(this.selectedChannel)] = this.channel;

        this.channels = channels;
        this.channel = null;
        this.displayDialog = false;
  }

  handleResponse(channels: any) : void
  {
      this.channels = channels;
      this.isRequesting = false;
  }

  handleError(error: any): void
  {
      this.isRequesting = false;
      console.log(error);
  }
}

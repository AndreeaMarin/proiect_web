import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';


import { Playlist } from './playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  private playlistUrl = 'http://localhost:3000/api/playlists';  // URL to web api
  private playlistItemUrl = 'http://localhost:3000/api/playlistItem';  // URL to web api

   private changedPlaylistSource  = new Subject<Playlist[]>();
   private editedPlaylistSource   = new Subject<Playlist>();

   changedPlaylist$ = this.changedPlaylistSource.asObservable();
   editedPlaylist$  = this.editedPlaylistSource.asObservable();

  httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true
  };

   messages: string[] = [];


  constructor(
   private http: HttpClient
   ) { }

   changedPlaylist(playlist: Playlist[])
   {
       this.changedPlaylistSource.next(playlist);
   }

   editedPlaylist(playlist: Playlist)
   {
       this.editedPlaylistSource.next(playlist);
   }

   getPlaylists(): Observable<Playlist[]> {
      // TODO: send the message _after_ fetching the heroes
      return this.http.get<Playlist[]>(this.playlistUrl, this.httpOptions)
            .pipe(
              tap(_ => this.log('fetched playlists')),
              catchError(this.handleError<Playlist[]>('getPlaylists', []))
            );
   }

    postPlaylist(playlist: Playlist): Observable<{}>
    {
       var obj = {
          "snippet" : {
              "title" : playlist.title,
              "description": playlist.description
          }
       };
       let body = JSON.stringify(obj);
       return this.http
               .post<Playlist>(this.playlistUrl, body, this.httpOptions);
    }

    putPlaylist(playlist: Playlist): Observable<{}>
    {
       var obj = {
          "id" : playlist.playlistId,
          "snippet" : {
              "title" : playlist.title,
              "description": playlist.description
          }
       };
       let body = JSON.stringify(obj);
       return this.http
               .put<Playlist>(this.playlistUrl, body, this.httpOptions);
    }

    deletePlaylist(playlist: Playlist): Observable<{}>
    {

     this.playlistUrl = this.playlistUrl + '/' + playlist.playlistId;
     return this.http
             .delete<Playlist>(this.playlistUrl, this.httpOptions);
    }

    addVideoToPlaylist(playlistId: string, videoId: string){
      var obj = {
        "snippet" : {
            "playlistId" : playlistId,
            "resourceId": {
                "kind": "youtube#video",
                "videoId": videoId
            }
        }
      };
      let body = JSON.stringify(obj);
      return this.http
             .post<any>(this.playlistItemUrl, body, this.httpOptions);
    }

   /**
      * Handle Http operation that failed.
      * Let the app continue.
      * @param operation - name of the operation that failed
      * @param result - optional value to return as the observable result
      */
     private handleError<T> (operation = 'operation', result?: T) {
       return (error: any): Observable<T> => {

         // TODO: send the error to remote logging infrastructure
         console.error(error); // log to console instead

         // TODO: better job of transforming error for user consumption
         this.log(`${operation} failed: ${error.message}`);

         // Let the app keep running by returning an empty result.
         return of(result as T);
       };
     }

     add(message: string) {
        this.messages.push(message);
     }

     private log(message: string) {
       this.add(`PlaylistsService: ${message}`);
     }
}

import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { Playlist } from '../playlist';

import { PlaylistsService } from '../playlists.service';

@Component({
  selector: 'playlists-dashboard',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.css']
})
export class PlaylistsComponent implements OnInit {

  playlists: Playlist[];

  selectedPlaylist: Playlist;

  newPlaylist: boolean;

  cols: any[];

  playlist: Playlist;

  displayDialog: boolean;

  isRequesting: boolean;


  constructor(  private playlistService: PlaylistsService ) {
      playlistService.changedPlaylist$.subscribe(
          playlists => {
              this.playlists = playlists;
          }
      );
  }

  ngOnInit() {
      this.playlists = [];
      this.getPlaylists();
  }

  getPlaylists(): void {
    this.isRequesting = true;

    this.playlistService.getPlaylists()
        .subscribe(
           data => this.handleResponse(data),
           error => this.handleError(error)
        );
  }

  showDialogToAdd() {
      this.newPlaylist = true;
      this.playlist = {playlistId:'',title:'',description:'',itemCount:0};
      this.displayDialog = true;
  }

  save() {
      let playlists = [...this.playlists];
      if (this.newPlaylist){
          this.playlistService.postPlaylist(this.playlist)
              .subscribe(playlists => {
                 this.playlists.push(this.playlist);
              });
          playlists.push(this.playlist);
      }else{
          this.playlistService.putPlaylist(this.playlist)
              .subscribe(playlists => {
                  playlists[this.playlists.indexOf(this.selectedPlaylist)] = this.playlist;
              });
          playlists[this.playlists.indexOf(this.selectedPlaylist)] = this.playlist;
      }

      this.playlists = playlists;
      this.playlist = null;
      this.displayDialog = false;
  }

  onRowSelect(event) {
      this.newPlaylist = false;
      this.playlist = this.clonePlaylist(event.data);
      this.displayDialog = true;
  }

  clonePlaylist(p: Playlist): Playlist {
      let playlist = {};
      for (let prop in p) {
          playlist[prop] = p[prop];
      }
      return playlist;
  }

  delete() {
      this.playlistService.deletePlaylist(this.playlist)
          .subscribe(playlists => {
              let index = this.playlists.indexOf(this.selectedPlaylist);
              this.playlists = this.playlists.filter((val, i) => i != index);
          });

      let index = this.playlists.indexOf(this.selectedPlaylist);
      this.playlists = this.playlists.filter((val, i) => i != index);
      this.playlist = null;
      this.displayDialog = false;
  }

  handleResponse(playlists: any) : void
  {
      this.playlists = playlists;
      this.isRequesting = false;
  }

  handleError(error: any): void
  {
      this.isRequesting = false;
      console.log(error);
  }

}

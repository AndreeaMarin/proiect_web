import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private verifyLoggedIn = 'http://localhost:3000/auth/is-user-logged-in';  // URL to web api
  private logoutUser = 'http://localhost:3000/auth/logout';  // URL to web api

  httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true
  };

  constructor(
    private http: HttpClient
  ) { }

  getUser (): Observable<User> {
    return this.http.get<User>(this.verifyLoggedIn, this.httpOptions);
  }

  deleteUser (): Observable<User> {
    return this.http.get<User>(this.logoutUser, this.httpOptions);
  }
}

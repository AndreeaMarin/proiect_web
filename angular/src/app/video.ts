export class Video {
  videoId?: string;
  title?: string;
  description?: string;
  thumbnails?:any;
  categoryId?: number;
}

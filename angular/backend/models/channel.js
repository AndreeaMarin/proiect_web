'use strict';
module.exports = (sequelize, DataTypes) => {
  const Channel = sequelize.define('Channel', {
    channelId: {
      type:DataTypes.STRING,
      primaryKey: true
    },
    userId: {
      type:DataTypes.STRING,
      allowNull: false
    },
    title: {
      type:DataTypes.STRING,
      allowNull: false
    },
    description: {
      type:DataTypes.TEXT,
      allowNull: false
    },
    subscriberCount:  {
      type:DataTypes.MEDIUMINT,
      allowNull: false
     }
  }, { sequelize, modelName: 'channel', tableName:'channels', timestamps:false, paranoid: true });
  Channel.associate = function(models) {
    Channel.belongsTo(models.User, {foreignKey:'userId', targetKey:'userId'});
    Channel.hasMany(models.Playlist, {sourceKey: 'channelId', foreignKey:'channelId'});
    Channel.hasMany(models.Video, {sourceKey: 'channelId', foreignKey:'channelId'});
  };
  return Channel;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    userId: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    accessToken: {
      type: DataTypes.STRING,
      allowNull: false
    },
    refreshToken: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, { sequelize, modelName: 'user', tableName:'users', timestamps:false, paranoid: true });
  User.associate = function(models) {
      User.hasMany(models.Channel,{sourceKey: 'userId', foreignKey:'userId'});
  };
  return User;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Video = sequelize.define('Video', {
    videoId: {
      type:DataTypes.STRING,
      primaryKey: true
    },
    title: {
      type:DataTypes.STRING,
      allowNull: false
    },
    description: {
      type:DataTypes.TEXT,
      allowNull: false
    },
    thumbnails: {
      type:DataTypes.TEXT,
      allowNull: false
    },
    categoryId: {
      type:DataTypes.MEDIUMINT,
      allowNull: false
    }
  }, { sequelize, modelName: 'video', tableName:'videos', timestamps:false, paranoid: true });
  Video.associate = function(models) {
      Video.belongsTo(models.Channel, {foreignKey:'channelId', targetKey:'channelId'});
  };
  return Video;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Playlist = sequelize.define('Playlist', {
    playlistId: {
      type:DataTypes.STRING,
      primaryKey: true
    },
    title:  {
      type:DataTypes.STRING,
      allowNull: false
    },
    description: {
      type:DataTypes.TEXT,
      allowNull: false
    },
    channelId: {
      type:DataTypes.STRING,
      allowNull: false
    },
    itemCount: {
      type:DataTypes.MEDIUMINT,
      allowNull: false
    }
  }, {
  indexes:[
       {
         unique: false,
         fields:['title']
       }
  ],
  sequelize,
  modelName: 'playlist',
  tableName:'playlists',
  timestamps:false,
  paranoid: true });
  Playlist.associate = function(models) {
    Playlist.belongsTo(models.Channel, {foreignKey:'channelId', targetKey:'channelId'});
  };
  return Playlist;
};

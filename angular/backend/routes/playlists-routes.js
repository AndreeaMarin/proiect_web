const router = require('express').Router();
const db = require('../models/index');
const Playlist = require('../models').Playlist;
const User = require('../models').User;
const Channel = require('../models').Channel;
const passport = require("passport");
const request = require('request');
const keys = require('../config/keys');

var refreshToken = function (req, res, next) {
  var accessToken = req.user.accessToken;
  const options = {
         url: 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + accessToken,
         method: 'GET',
         headers:{
           'Accept':'application/json'
         }
      };
  request(options, function(err, res, body){
    let json = JSON.parse(body);
    if(typeof json.expires_in === 'undefined' || json.expires_in <= 0){
      var user = User.findOne({ where: { userId: req.user.userId } }).then((currentUser)=> {
        const obj = {
            refresh_token: currentUser.refreshToken,
            client_id: keys.google.clientID,
            client_secret: keys.google.clientSecret,
            grant_type: 'refresh_token'
         };

        const options2 = {
             url: 'https://oauth2.googleapis.com/token',
             method: 'POST',
             headers:{
               'Accept':'application/json'
             },
             body: JSON.stringify(obj)
          };

          request(options2, function(er, rsp, bdy){
              let jsn = JSON.parse(bdy);
              if( typeof jsn.access_token !== undefined )
                currentUser.update({accessToken:jsn.access_token}).then((usr) => console.log(usr));
          });
      });
    }
  });
  next()
}

router.use(refreshToken);

router.get('/playlists', function(req, res, next){
    var accessToken = req.user.accessToken;
    const options = {
       url: 'https://www.googleapis.com/youtube/v3/playlists',
       method: 'GET',
       headers:{
         'Authorization':' Bearer ' + accessToken,
         'Accept':'application/json'
       },
       qs: {
         part: 'snippet,contentDetails',
         mine: 'true',
         maxResults: 50
       }
    };

    request(options, function(err, res, body){
       let json = JSON.parse(body);
       if(typeof json.items !== 'undefined' && json.items.length > 0){
         json.items.forEach((item,index)=>{
           Playlist.findOne({ where: { playlistId: item.id } }).then((currentPlaylist) => {
             if(!currentPlaylist){
               new Playlist({
                 playlistId: item.id,
                 title: item.snippet.title,
                 description: item.snippet.description,
                 channelId: item.snippet.channelId,
                 itemCount: item.contentDetails.itemCount
               }).save();
           }else if (currentPlaylist.itemCount !== item.contentDetails.itemCount || currentPlaylist.title !== item.snippet.title || currentPlaylist.description !== item.snippet.description){
              currentPlaylist.update({
                title: item.snippet.title,
                description: item.snippet.description,
                itemCount: item.contentDetails.itemCount
              });
           }});
         });
       }
    });

    Playlist.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((playlists)=> {
        res.json(playlists);
    });

});

router.post('/playlists', function(req, res, next){
  if(typeof req.user !== 'undefined'){
      var accessToken = req.user.accessToken;
      const options = {
         url: 'https://www.googleapis.com/youtube/v3/playlists',
         method: 'POST',
         headers:{
           'Authorization':' Bearer ' + accessToken,
           'Accept':'application/json',
           'Content-Type':'application/json'
         },
         qs: {
           part: 'snippet'
         },
         body: req.body,
         json: true
      };

    request(options, function(err, res, body){
      if(typeof body.id !== 'undefined'){
        Playlist.findOne({ where: { playlistId: body.id } }).then((currentPlaylist) => {
          if(!currentPlaylist){
            new Playlist({
              playlistId: body.id,
              title: body.snippet.title,
              description: body.snippet.description,
              channelId: body.snippet.channelId,
              itemCount: 0
            }).save().then((newPlaylist) => {
               done(null, newPlaylist);
             });
        }});
      }else{
        console.log(err);
      }
    });
    Playlist.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((playlists)=> {
        res.json(playlists);
    });
  }
});

router.put('/playlists', function(req, res, next){
  if(typeof req.user !== 'undefined'){
      var accessToken = req.user.accessToken;
      const options = {
         url: 'https://www.googleapis.com/youtube/v3/playlists',
         method: 'PUT',
         headers:{
           'Authorization':' Bearer ' + accessToken,
           'Accept':'application/json',
           'Content-Type':'application/json'
         },
         qs: {
           part: 'snippet'
         },
         body: req.body,
         json: true
      };

    request(options, function(err, res, body){
      console.log(body);
      if(typeof body.id !== 'undefined'){
        Playlist.findOne({ where: { playlistId: body.id } }).then((currentPlaylist) => {
            currentPlaylist.update({
              title: body.snippet.title,
              description: body.snippet.description
            });
        });
      }else{
        console.log(err);
      }
    });
    Playlist.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((playlists)=> {
        res.json(playlists);
    });
  }
});

router.delete('/playlists/:id', function(req, res, next){
  if(typeof req.user !== 'undefined'){
      var accessToken = req.user.accessToken;
      const options = {
         url: 'https://www.googleapis.com/youtube/v3/playlists',
         method: 'DELETE',
         headers:{
           'Authorization':' Bearer ' + accessToken,
           'Accept':'application/json',
           'Content-Type':'application/json'
         },
         qs: {
           id: req.params.id
         },
         json: true
      };

    request(options, function(err, res, body){
      console.log(res.statusCode);
      if(res.statusCode == 204){
        Playlist.destroy({ where: { playlistId: req.params.id } });
      }else{
        console.log(err);
      }
    });
    Playlist.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((playlists)=> {
        res.json(playlists);
    });
  }
});

router.post('/playlistItem', function(req, res, next){
  if(typeof req.user !== 'undefined'){
      var accessToken = req.user.accessToken;
      const options = {
         url: 'https://www.googleapis.com/youtube/v3/playlistItems',
         method: 'POST',
         headers:{
           'Authorization':' Bearer ' + accessToken,
           'Accept':'application/json',
           'Content-Type':'application/json'
         },
         qs: {
           part: 'snippet'
         },
         body: req.body,
         json: true
      };
      console.log(options);
      request(options, function(err, res, body){
          console.log(body);
          console.log(err);
      });
  }
});



module.exports = router

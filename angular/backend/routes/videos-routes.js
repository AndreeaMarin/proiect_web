const router = require('express').Router();
const db = require('../models/index');
const Video = require('../models').Video;
const User = require('../models').User;
const Channel = require('../models').Channel;
const passport = require("passport");
const request = require('request');
const keys = require('../config/keys');
const {google} = require('googleapis');
const sampleClient = require('../config/sampleclient');
const runSample = require('../config/fileUploader.js');
const path = require('path');
const fs = require('fs')


var refreshToken = function (req, res, next) {
  var accessToken = req.user.accessToken;
  const options = {
         url: 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + accessToken,
         method: 'GET',
         headers:{
           'Accept':'application/json'
         }
      };
  request(options, function(err, res, body){
    let json = JSON.parse(body);
    if(typeof json.expires_in === 'undefined' || json.expires_in <= 0){
      var user = User.findOne({ where: { userId: req.user.userId } }).then((currentUser)=> {
        const obj = {
            refresh_token: currentUser.refreshToken,
            client_id: keys.google.clientID,
            client_secret: keys.google.clientSecret,
            grant_type: 'refresh_token'
         };

        const options2 = {
             url: 'https://oauth2.googleapis.com/token',
             method: 'POST',
             headers:{
               'Accept':'application/json'
             },
             body: JSON.stringify(obj)
          };

          request(options2, function(er, rsp, bdy){
              let jsn = JSON.parse(bdy);
              if( typeof jsn.access_token !== undefined )
                currentUser.update({accessToken:jsn.access_token}).then((usr) => console.log(usr));
          });
      });
    }
  });
  next()
}

router.use(refreshToken);

router.get('/videoCategories', function(req, res, next){
    var accessToken = req.user.accessToken;
    const options = {
       url: 'https://www.googleapis.com/youtube/v3/videoCategories',
       method: 'GET',
       headers:{
         'Authorization':' Bearer ' + accessToken,
         'Accept':'application/json'
       },
       qs: {
         part: 'snippet',
         hl: 'en_US',
         regionCode: 'RO'
       }
    };

    request(options, function(err, rs, body){
       let json = JSON.parse(body);
       if(typeof json.items !== 'undefined' && json.items.length > 0){
          res.send(json.items);
       }else{
          console.log(err);
       }
    });
});

router.get('/videos', function(req, res, next){
    Video.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((videos)=> {
        res.json(videos);
    });
});

router.post('/videos', function(req, res, next){
    var accessToken = req.user.accessToken;

    const youtube = google.youtube({
        version: 'v3',
        auth: sampleClient.oAuth2Client,
    });

    const part = 'id,snippet,status';

    const title = req.get('X-Video-Title');
    const description = req.get('X-Video-Description');
    const categoryId = req.get('X-Video-Category');

    const requestBody = {
        snippet: {
          title: title,
          description: description,
          categoryId: parseInt(categoryId)
        },
        status: {
          privacyStatus: 'private',
        }
    };

    const scopes = [
      'https://www.googleapis.com/auth/youtube.upload',
      'https://www.googleapis.com/auth/youtube',
    ];


//file storage and creation on server
    let videoFile = req.files.video;
    let videoPath = path.resolve('./uploads', videoFile.name);

    videoFile.mv(videoPath, function(err) {
      if (err)
        return res.status(500).send(err);

      console.log(req.user);

    //file uploading towards youtube
      sampleClient
          .authenticate(scopes, req.user)
          .then(() => {
              runSample(videoPath, part, requestBody).then((data)=>{
                  new Video({
                       videoId: data.id,
                       title: data.snippet.title,
                       channelId: data.snippet.channelId,
                       description: data.snippet.description,
                       thumbnails: JSON.stringify(data.snippet.thumbnails),
                       categoryId: data.snippet.categoryId
                  }).save();

                  fs.unlink(videoPath, (err) => {
                    if (err) {
                      console.error(err)
                      return
                    }

                    console.log(videoPath + ' was successfully deleted');
                  });

                  res.sendStatus(200);
              })
          })
          .catch((error) =>{
              console.log(error);
              fs.unlink(videoPath, (err) => {
                if (err) {
                  console.error(err)
                  return
                }

                console.log(videoPath + ' was successfully deleted');
              });
          });
    });
});

router.put('/videos', function(req, res, next){
    var accessToken = req.user.accessToken;
    const options = {
       url: 'https://www.googleapis.com/youtube/v3/videos',
       method: 'PUT',
       headers:{
         'Authorization':' Bearer ' + accessToken,
         'Accept':'application/json'
       },
        qs: {
            part: 'id,snippet'
        },
        body: req.body,
        json: true
    };

    request(options, function(err, res, body){
      console.log(body);
      if(typeof body.id !== 'undefined'){
        Video.findOne({ where: { videoId: body.id } }).then((currentVideo) => {
            currentVideo.update({
                title: body.snippet.title,
                description: body.snippet.description
            });
        });
      }else{
        console.log(err);
      }
    });
    Video.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((videos)=> {
        res.json(videos);
    });
});

router.delete('/videos/:id', function(req, res, next){
    var accessToken = req.user.accessToken;
    const options = {
       url: 'https://www.googleapis.com/youtube/v3/videos',
       method: 'DELETE',
       headers:{
          'Authorization':' Bearer ' + accessToken,
          'Accept':'application/json',
          'Content-Type':'application/json'
       },
       qs: {
          id: req.params.id
       },
       json: true
    };
    request(options, function(err, res, body){
      console.log(res.statusCode);
      if(res.statusCode == 204){
        Video.destroy({ where: { videoId: req.params.id } });
      }else{
        console.log(err);
      }
    });
    Video.findAll({
          include: [{
              model: Channel,
              where: { userId: req.user.userId }
          }]
    }).then((videos)=> {
        res.json(videos);
    });
});

module.exports = router





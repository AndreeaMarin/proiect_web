const router = require('express').Router();
const db = require('../models/index');
const Channel = require('../models').Channel;
const User = require('../models').User;
const passport = require("passport");
const request = require('request');
const keys = require('../config/keys');
const Sequelize = require("sequelize");


var refreshToken = function (req, res, next) {
  var accessToken = req.user.accessToken;
  const options = {
         url: 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + accessToken,
         method: 'GET',
         headers:{
           'Accept':'application/json'
         }
      };
  request(options, function(err, res, body){
    let json = JSON.parse(body);
    if(typeof json.expires_in === 'undefined' || json.expires_in <= 0){
      var user = User.findOne({ where: { userId: req.user.userId } }).then((currentUser)=> {
        const obj = {
            refresh_token: currentUser.refreshToken,
            client_id: keys.google.clientID,
            client_secret: keys.google.clientSecret,
            grant_type: 'refresh_token'
         };

        const options2 = {
             url: 'https://oauth2.googleapis.com/token',
             method: 'POST',
             headers:{
               'Accept':'application/json'
             },
             body: JSON.stringify(obj)
          };

          request(options2, function(er, rsp, bdy){
              let jsn = JSON.parse(bdy);
              if( typeof jsn.access_token !== undefined )
                currentUser.update({accessToken:jsn.access_token}).then((usr) => console.log(usr));
          });
      });
    }
  });
  next()
}

router.use(refreshToken);

router.get('/channels', function(req, res, next){
    var accessToken = req.user.accessToken;
    const options = {
       url: 'https://www.googleapis.com/youtube/v3/channels',
       method: 'GET',
       headers:{
         'Authorization':' Bearer ' + accessToken,
         'Accept':'application/json'
       },
       qs: {
         part: 'snippet,contentDetails,statistics',
         mine: 'true'
       }
    };
    request(options, function(err, res, body){
      let json = JSON.parse(body);
      if(typeof json.items !== 'undefined' && json.items.length > 0){
        json.items.forEach((item,index)=>{
          Channel.findOne({ where: { channelId: item.id } }).then((currentChannel) => {
            if(!currentChannel){
              new Channel({
                channelId: item.id,
                userId: req.user.userId,
                title: item.snippet.title,
                description: item.snippet.description,
                subscriberCount: item.statistics.subscriberCount
              }).save();
          }else if(currentChannel.description !== item.snippet.description || currentChannel.title !== item.snippet.title){
            currentChannel.update({
                          description: item.snippet.description,
                          title:item.snippet.title
            });
          }
          });
        });
      }
    });

    Channel.findAll({
          include: [{
              model: User,
              where: { userId: req.user.userId }
          }]
    }).then((channels)=> {
        res.json(channels);
    });
});

router.put('/channels', function(req, res, next){
  if(typeof req.user !== 'undefined'){
      var accessToken = req.user.accessToken;
      const options = {
         url: 'https://www.googleapis.com/youtube/v3/channels',
         method: 'PUT',
         headers:{
           'Authorization':' Bearer ' + accessToken,
           'Accept':'application/json',
           'Content-Type':'application/json'
         },
         qs: {
           part: 'brandingSettings'
         },
         body: req.body,
         json: true
      };

    console.log(options);
    request(options, function(err, res, body){
      console.log(body);
      if(typeof body.id !== 'undefined'){
        Channel.findOne({ where: { channelId: body.id } }).then((currentChannel) => {
            currentChannel.update({
              description: body.brandingSettings.channel.description,
              title:body.brandingSettings.channel.title
            });
        });
      }else{
        console.log(err);
      }
    });
    Channel.findAll({
          include: [{
              model: User,
              where: { userId: req.user.userId }
          }]
    }).then((channels)=> {
        res.json(channels);
    });
  }
});

module.exports = router

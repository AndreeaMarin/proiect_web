'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Playlists', {
      playlistId: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      channelId: {
        type: Sequelize.STRING,
        references: {
          model: 'Channels',
          key: 'channelId'
        },
        allowNull: false
      },
      itemCount: {
        type: Sequelize.MEDIUMINT,
        allowNull: false
      }
    }).then(() => queryInterface.addIndex('Playlists', ['title']))
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Playlists');
  }
};

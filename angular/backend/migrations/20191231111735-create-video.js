'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Videos', {
      videoId: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      channelId: {
        type: Sequelize.STRING,
        references: {
          model: 'Channels',
          key: 'channelId'
       },
        allowNull: false
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      thumbnails: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      categoryId: {
        type: Sequelize.MEDIUMINT,
        allowNull: false
     }
    }).then(() => queryInterface.addIndex('Videos', ['title']));
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Videos');
  }
};

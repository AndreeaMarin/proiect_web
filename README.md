**Author: Andreea Ionela Marin**


# Aplicatie manager videoclipuri favorite integrata cu Youtube

# Obiectiv

Realizarea unei aplicatii web pentru a putea gestiona videoclipurile favorite si nu numai, integrata cu YouTube.

# Descriere
Aplicatia permite crearea unui cont prin care utilizatorul isi poate organiza playlist-ul de videoclipuri in functie de preferintele acestuia.
De asemenea, utilizatorul poate printr-un formular sa-si incarce un videoclip in canalele sale, sa-si actualizeze informatia videoclipului si sa-si gestioneze canalul.

# Functionalitati
1. Ca un utilizator as vrea sa pot sa-mi creez listele mele personalizate de videoclipuri;
2. Ca un utilizator as vrea sa pot sa-mi sterg playlisturile mele;
3. Ca un utilizator as vrea sa pot sa-mi modific playlisturile in functie de preferintele mele;
4. Ca un utilizator as vrea sa pot sa-mi listez playlisturile;
5. Ca un utilizator as vrea sa pot sa caut videoclipuri sa se le adaug in playlistul meu favorit;
6. Ca un utilizator as vrea sa pot sa-mi incarc video-ul preferat in canalul meu;
7. Ca un utilizator as vrea sa pot sa-mi actualizez informatiile video-ului incarcat;
8. Ca un utilizator as vrea sa pot sa-mi actualizez informatia canalului meu;

## Instalarea programului si pornirea programului


1. Instalarea dependintelor se face ruland in directorul root al rest api-ului (backend), comanda:

```javascript
cd angular/backend
npm install
```
2. Instalarea dependintelor se face ruland in directorul root al proiectului (angular), comanda:

```javascript
cd angular
npm install
```

Aceasta dupa instalare va porni si aplicatia Angular cat si backendul REST.
Baza de date si migrariile vor fi rulate la final, tot in cadrul acestei etape.

## Dezvoltarea unei aplicații web cu arhitectură frontend - backend

Arhitectura este prezentata în trei părți:

1. [Arhitectura aplicației](arhitectura-aplicatiei.md)
2. [Construirea API-ului RESTful folosind ExpressJS](rest-api.md)
3. [Construirea interfeței folosind HTML, Angular, JQuery](frontend.md)

Codul sursă rezultat este disponiblil pe bitbucket în directorul [proiect_web](https://bitbucket.org/AndreeaMarin/proiect_web).


## Operații pe date și persistență

În acest tutorial sunt exemplificate operațiile pe date și persistența într-o bază de date relațională

1. [ORM (Object-relational mapping) cu Sequelize](orm.md)


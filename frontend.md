**Author: Andreea Ionela Marin**


# Construirea interfeței folosind HTML, Angular, JQuery

# Structura aplicatiei:

Aplicatia este dezvoltata pe componente, fiecare view/entitate avand propria pagina in care utilizatorul poate efectua operatii CRUD

![](./structure1.png)

![](./structure2.png)

Fiecare componenta apeleaza un serviciu dedicat.

Fiecare serviciu dedicat se ocupa strict cu incarcarea datelor de la API efectuand operatii de GET, POST, PUT, DELETE peste clientul de http oferit de angular.

Componenta principala de intrare este app.component, in ea sunt afisate linkurile care duc la fiecare componenta in parte.

De asemenea, aplicatia foloseste un tool de templetizare numit primeng, care pune la disponibilitatea aplicatiei diverse elemente de design ( butoane, formulare, liste, etc.)

Interfata aplicatiei:

![](./interfata.png)


